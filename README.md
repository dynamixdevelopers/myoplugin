The Myo Plugin allows interaction with the Myo gesture recognition armband.

All interaction is performed through the Ambient Control mechanisms. See [Ambient Control documentation](https://bitbucket.org/dynamixdevelopers/ambientcontrolplugin)

The plugins control description is as follows:
```JSON
{
    "name":"myoplugin",
    "artifact_id":"org.ambientdynamix.contextplugins.myoplugin",
    "description":"",
    "pluginVersion":"1.0.0",
    "owner_id":"",
    "platform":"",
    "minPlatformVersion":"",
    "minFrameworkVersion":"",
    "inputList":[],
    "outputList":{
        "Switch":"SWITCH",
        "Backward seek":"PLAYBACK_BACKWARD_SEEK",
        "Forward seek":"PLAYBACK_FORWARD_SEEK",
        "IMU":"SENSOR_PYR",
        "Stop":"PLAYBACK_STOP"
    },
    "optionalInputList":[
        "SENSOR_TOGGLE"
     ]
}
```